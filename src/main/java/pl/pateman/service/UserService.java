package pl.pateman.service;

import pl.pateman.document.User;

/**
 * Created by pateman.
 */
public interface UserService {
    User createUser(final String username, final String password);
    User setPassword(final User user, final String password);
    void ensureAdmin();
}
