package pl.pateman.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.pateman.document.User;
import pl.pateman.repository.UserRepository;
import pl.pateman.service.UserService;

/**
 * Created by pateman.
 */
@Service("userService")
public class UserServiceImpl implements UserService {
    private static final String ADMIN = "admin";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User createUser(String username, String password) {
        final User newUser = new User();
        newUser.setUsername(username);
        this.setPassword(newUser, password);

        this.userRepository.save(newUser);
        return newUser;
    }

    @Override
    public User setPassword(User user, String password) {
        user.setPassword(this.passwordEncoder.encode(password));
        return user;
    }

    @Override
    public void ensureAdmin() {
        if (this.userRepository.findByUsername(ADMIN) == null) {
            this.createUser(ADMIN, ADMIN);
        }
    }


}
