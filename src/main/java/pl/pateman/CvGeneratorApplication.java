package pl.pateman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import pl.pateman.service.UserService;

@SpringBootApplication
public class CvGeneratorApplication {

	public static void main(String[] args) {
		final ConfigurableApplicationContext context = SpringApplication.run(CvGeneratorApplication.class, args);
		final UserService userService = (UserService) context.getBean("userService");
		userService.ensureAdmin();
	}
}
