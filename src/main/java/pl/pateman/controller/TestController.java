package pl.pateman.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by pateman.
 */
@Controller
public class TestController {
    @RequestMapping("/")
    public @ResponseBody String index() {
        return "Hello world!";
    }
}
