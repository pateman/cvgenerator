package pl.pateman.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import pl.pateman.document.User;

/**
 * Created by pateman.
 */
public interface UserRepository extends MongoRepository<User, String> {
    User findByUsername(String username);
}
